/*
Navicat MySQL Data Transfer

Source Server         : dstar_store
Source Server Version : 50146
Source Host           : localhost:3306
Source Database       : dstar_store

Target Server Type    : MYSQL
Target Server Version : 50146
File Encoding         : 65001

Date: 2018-07-13 14:50:29
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `activity`
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `activityID` int(15) NOT NULL AUTO_INCREMENT,
  `activityname` varchar(50) NOT NULL,
  `productID` int(20) NOT NULL,
  `content` varchar(200) NOT NULL,
  `startday` date NOT NULL,
  `endday` date NOT NULL,
  `state` int(2) NOT NULL,
  `activityImage` varchar(100) DEFAULT NULL,
  `brand` varchar(30) NOT NULL,
  PRIMARY KEY (`activityID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('20', '1231', '15', '4564', '2018-07-04', '2018-07-07', '4', 'img/banner/banner2.png', '三星');
INSERT INTO `activity` VALUES ('0', 'asdf', '15', 'dsfa', '2018-07-09', '2018-07-18', '2', 'img/banner/banner4.png', '2');
INSERT INTO `activity` VALUES ('3', 'gfdh', '15', 'dsaf', '2018-07-04', '2018-07-12', '2', 'img/banner/banner5.png', 'toyota');
INSERT INTO `activity` VALUES ('6', 'dsafsda', '15', 'dsfa', '2018-05-08', '2018-06-22', '2', 'img/banner/banner6.png', 'adidas');
INSERT INTO `activity` VALUES ('7', '华为大减价', '15', '华为', '2018-07-17', '2018-07-20', '1', 'img/banner/banner7.png', 'HUAWEI');
INSERT INTO `activity` VALUES ('8', '苹果', '15', '苹果', '2018-07-25', '2018-07-27', '0', 'img/banner/banner8.png', 'xiexie ');
INSERT INTO `activity` VALUES ('10', 'iphone', '15', 'test', '2000-10-08', '2000-12-05', '2', 'img/banner/banner9.png', 'apple');
INSERT INTO `activity` VALUES ('4', 'ssssssss', '15', 'dsaf', '2015-11-02', '2016-01-01', '2', 'img/banner/banner10.png', 'iphone');
INSERT INTO `activity` VALUES ('2', '55555555555', '15', '555555555555', '2018-07-02', '2018-07-20', '1', 'img/banner/banner1.png', 'apple');
INSERT INTO `activity` VALUES ('1', 'Apple大促销', '15', '轻薄简约时尚 5折优惠', '2018-07-04', '2018-07-28', '1', 'img/activity/1531498135561.jpg', 'Apple');

-- ----------------------------
-- Table structure for `address`
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `addressID` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `state` int(2) NOT NULL,
  PRIMARY KEY (`addressID`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('10', 'zss', '王泓森', '河北省唐山市遵化市建华南路172号开元风景5-2-1202', '0');
INSERT INTO `address` VALUES ('7', 'zss', '圣地亚哥', '纽约市诺克萨斯州圣地亚市麦克林街道698号路南', '0');
INSERT INTO `address` VALUES ('16', '18821635187', 'ijiage', '陕西省咸阳市杨凌区西北农林科技大学北校区西区3号楼', '0');
INSERT INTO `address` VALUES ('18', '18821635187', 'dfhr', '陕西省咸阳市杨凌区西北农林科技大学北校区西区3号楼', '0');
INSERT INTO `address` VALUES ('24', '18821635187', '67', 'sdgwegawe', '0');
INSERT INTO `address` VALUES ('25', '18821635187', 'aewg', 'sadgargrs', '0');
INSERT INTO `address` VALUES ('23', '18821635187', 'jisang', 'awegawegasd', '0');
INSERT INTO `address` VALUES ('21', '18821635187', '阿斯顿肌酐', '阿斯顿噶是的噶十多个阿根深蒂固', '0');

-- ----------------------------
-- Table structure for `blogcomment`
-- ----------------------------
DROP TABLE IF EXISTS `blogcomment`;
CREATE TABLE `blogcomment` (
  `commentID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `name` varchar(20) NOT NULL,
  `comment` varchar(50) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blogcomment
-- ----------------------------
INSERT INTO `blogcomment` VALUES ('61', 'Wonderful', 'su', '去见你想见的人吧。趁阳光正好，趁微风不噪，趁繁花还未开至荼蘼，趁现在还年轻，还可以走很长很长的路', '2018-07-13 08:40:41');
INSERT INTO `blogcomment` VALUES ('59', 'Wonderful', 'zss', 'ghg', '2018-07-12 21:52:17');
INSERT INTO `blogcomment` VALUES ('60', 'Wonderful', 'zss', 'hello', '2018-07-13 08:36:30');
INSERT INTO `blogcomment` VALUES ('58', 'Wonderful', 'zss', 'good', '2018-07-12 21:08:10');
INSERT INTO `blogcomment` VALUES ('62', 'Wonderful', 'zss', 'jsdklasdjsk', '2018-07-13 10:49:34');

-- ----------------------------
-- Table structure for `blogreply`
-- ----------------------------
DROP TABLE IF EXISTS `blogreply`;
CREATE TABLE `blogreply` (
  `replyID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `comment` varchar(50) NOT NULL,
  `time` varchar(30) NOT NULL,
  `id` int(10) NOT NULL,
  PRIMARY KEY (`replyID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blogreply
-- ----------------------------
INSERT INTO `blogreply` VALUES ('14', 'Wonderful', 'zss', 'adasd', '2018-07-13 10:49:44', '61');

-- ----------------------------
-- Table structure for `cart`
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cartID` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `totalprice` float(15,2) NOT NULL,
  `count` int(3) DEFAULT NULL,
  PRIMARY KEY (`cartID`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('1', 'zhangyun2', '16010.00', '2');
INSERT INTO `cart` VALUES ('2', '2015012784', '0.00', '0');
INSERT INTO `cart` VALUES ('3', 'su', '16010.00', '2');
INSERT INTO `cart` VALUES ('30', 'zss', '0.00', '0');
INSERT INTO `cart` VALUES ('8', '2034', '0.00', '0');
INSERT INTO `cart` VALUES ('5', 'Bill', '1298.00', '1');
INSERT INTO `cart` VALUES ('6', 'sususu', '0.00', '0');
INSERT INTO `cart` VALUES ('7', 'susu', '0.00', '0');
INSERT INTO `cart` VALUES ('18', '18821715048', '0.00', '0');
INSERT INTO `cart` VALUES ('9', 'wl', '0.00', '0');
INSERT INTO `cart` VALUES ('10', '18821635187', '0.00', '0');
INSERT INTO `cart` VALUES ('11', '18821635187', '0.00', '0');
INSERT INTO `cart` VALUES ('31', '15229315883', '0.00', '0');
INSERT INTO `cart` VALUES ('29', '17791389021', '9999.00', '1');
INSERT INTO `cart` VALUES ('32', '18821701702', '0.00', '0');
INSERT INTO `cart` VALUES ('0', '18821702370', '0.00', '0');
INSERT INTO `cart` VALUES ('34', '18821702370', '0.00', '0');

-- ----------------------------
-- Table structure for `cartitem`
-- ----------------------------
DROP TABLE IF EXISTS `cartitem`;
CREATE TABLE `cartitem` (
  `itemID` int(15) NOT NULL AUTO_INCREMENT,
  `cartID` int(15) NOT NULL,
  `productID` int(15) NOT NULL,
  `quantity` int(15) DEFAULT NULL,
  `productname` varchar(30) NOT NULL,
  `productPrice` float(30,2) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`itemID`),
  KEY `cartID` (`cartID`),
  KEY `productID` (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cartitem
-- ----------------------------
INSERT INTO `cartitem` VALUES ('35', '3', '2', '7', 'Phone1', '2000.50', '1');
INSERT INTO `cartitem` VALUES ('34', '3', '1', '3', 'Phone2', '2.00', '1');
INSERT INTO `cartitem` VALUES ('59', '5', '14', '3', 'Daysky DK 190', '1298.00', 'img/products/indexImage/L9.jpg');
INSERT INTO `cartitem` VALUES ('77', '15', '6', '2', '荣耀10', '2599.00', 'img/products/indexImage/p1.jpg');
INSERT INTO `cartitem` VALUES ('75', '18', '13', '3', '神舟 战神Z7-KPG', '7088.00', 'img/products/indexImage/L5.jpg');
INSERT INTO `cartitem` VALUES ('76', '18', '15', '3', 'MacBook Air', '9999.00', 'img/products/apple1.jpg');
INSERT INTO `cartitem` VALUES ('70', '24', '24', '1', 'Apple iPad 平板电脑', '2488.00', 'img/products/indexImage/T2.jpg');
INSERT INTO `cartitem` VALUES ('78', '26', '15', '3', 'MacBook Air', '9999.00', 'img/products/apple1.jpg');
INSERT INTO `cartitem` VALUES ('81', '29', '15', '3', 'MacBook Air', '9999.00', 'img/products/apple1.jpg');

-- ----------------------------
-- Table structure for `favorite`
-- ----------------------------
DROP TABLE IF EXISTS `favorite`;
CREATE TABLE `favorite` (
  `favoriteID` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `productID` int(15) NOT NULL,
  PRIMARY KEY (`favoriteID`),
  KEY `username` (`username`),
  KEY `productID` (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of favorite
-- ----------------------------
INSERT INTO `favorite` VALUES ('6', 'zss', '5');
INSERT INTO `favorite` VALUES ('21', 'zss', '13');
INSERT INTO `favorite` VALUES ('8', '', '6');
INSERT INTO `favorite` VALUES ('9', '18821635187', '19');
INSERT INTO `favorite` VALUES ('10', '18821635187', '6');

-- ----------------------------
-- Table structure for `messagerecord`
-- ----------------------------
DROP TABLE IF EXISTS `messagerecord`;
CREATE TABLE `messagerecord` (
  `messageID` int(15) NOT NULL AUTO_INCREMENT,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `content` varchar(200) NOT NULL,
  `time` date DEFAULT NULL,
  PRIMARY KEY (`messageID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of messagerecord
-- ----------------------------

-- ----------------------------
-- Table structure for `orderitem`
-- ----------------------------
DROP TABLE IF EXISTS `orderitem`;
CREATE TABLE `orderitem` (
  `itemID` int(15) NOT NULL AUTO_INCREMENT,
  `orderID` bigint(50) NOT NULL,
  `productID` int(15) NOT NULL,
  `quantity` int(5) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `productname` varchar(200) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`itemID`),
  KEY `orderID` (`orderID`),
  KEY `productID` (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderitem
-- ----------------------------
INSERT INTO `orderitem` VALUES ('235', '20180713093836', '13', '2', '未评价', '神舟 战神Z7-KPG', 'img/products/indexImage/L5.jpg');
INSERT INTO `orderitem` VALUES ('236', '20180713093928', '13', '2', '未评价', '神舟 战神Z7-KPG', 'img/products/indexImage/L5.jpg');
INSERT INTO `orderitem` VALUES ('237', '20180713093928', '14', '1', '未评价', 'Daysky DK 190', 'img/products/indexImage/L9.jpg');
INSERT INTO `orderitem` VALUES ('238', '20180713093955', '13', '2', '未评价', '神舟 战神Z7-KPG', 'img/products/indexImage/L5.jpg');
INSERT INTO `orderitem` VALUES ('239', '20180713093955', '14', '1', '未评价', 'Daysky DK 190', 'img/products/indexImage/L9.jpg');
INSERT INTO `orderitem` VALUES ('240', '20180713104859', '43', '1', '未评价', '华为 Matebook X WT-W09 i5', 'img/products/huawei1.jpg');
INSERT INTO `orderitem` VALUES ('241', '20180713105242', '15', '1', '未评价', 'MacBook Air', 'img/products/apple1.jpg');
INSERT INTO `orderitem` VALUES ('242', '20180713105424', '15', '1', '未评价', 'MacBook Air', 'img/products/apple1.jpg');
INSERT INTO `orderitem` VALUES ('243', '20180713110324', '15', '1', '未评价', 'MacBook Air', 'img/products/apple1.jpg');
INSERT INTO `orderitem` VALUES ('244', '20180713110652', '15', '1', '未评价', 'MacBook Air', 'img/products/apple1.jpg');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orderID` bigint(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `totalPrice` double(20,0) NOT NULL,
  `address` varchar(50) NOT NULL,
  `state` varchar(20) NOT NULL,
  `time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`orderID`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('20180713093836', 'zss', '14176', '未选定', '待付款', 'Fri Jul 13 09:38:36 CST 2018');
INSERT INTO `orders` VALUES ('20180713093928', 'zss', '15474', '河北省唐山市遵化市建华南路172号开元风景5-2-1202(收货人：王泓森)', '已付款', 'Fri Jul 13 09:39:28 CST 2018');
INSERT INTO `orders` VALUES ('20180713093955', 'zss', '15474', '纽约市诺克萨斯州圣地亚市麦克林街道698号路南(收货人：圣地亚哥)', '已完成', 'Fri Jul 13 09:39:55 CST 2018');
INSERT INTO `orders` VALUES ('20180713104859', 'zss', '5988', '未选定', '待付款', 'Fri Jul 13 10:48:59 CST 2018');
INSERT INTO `orders` VALUES ('20180713105242', 'zss', '9999', '河北省唐山市遵化市建华南路172号开元风景5-2-1202(收货人：王泓森)', '已付款', 'Fri Jul 13 10:52:42 CST 2018');
INSERT INTO `orders` VALUES ('20180713105424', 'zss', '9999', '未选定', '待付款', 'Fri Jul 13 10:54:24 CST 2018');
INSERT INTO `orders` VALUES ('20180713110324', 'zss', '9999', '河北省唐山市遵化市建华南路172号开元风景5-2-1202(收货人：王泓森)', '已付款', 'Fri Jul 13 11:03:24 CST 2018');
INSERT INTO `orders` VALUES ('20180713110652', 'zss', '9999', '河北省唐山市遵化市建华南路172号开元风景5-2-1202(收货人：王泓森)', '待付款', 'Fri Jul 13 11:06:52 CST 2018');

-- ----------------------------
-- Table structure for `post`
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `postRecordID` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `title` varchar(30) NOT NULL,
  `content` varchar(200) NOT NULL,
  `quote` varchar(50) NOT NULL,
  `pageview` int(5) NOT NULL DEFAULT '0',
  `time` varchar(30) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `type` varchar(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`postRecordID`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('26', 'su', 'I-phone', 'I-phone是一种智能手机。', '你知道i-phone?', '0', '2018-07-12 19:33:51', '../img/blog/1531395231323i-phone.jpg', '1');
INSERT INTO `post` VALUES ('27', 'zss', '热爱生活', '信心是要创始成终，信心是要创始成终，仰望耶稣走天路中，脱离撒旦古龙。 未见之事当作实底，许多应许在与真理，勿看眼前世上福气，要往新天新地。', '生活无处不美好', '0', '2018-07-12 19:43:04', '../img/blog/b12.jpg', '1');
INSERT INTO `post` VALUES ('28', 'wl', 'soccer', '北京时间7月15日23：00，俄罗斯世界杯将迎最终的决赛，法国将与克罗地亚争夺大力神杯，我们拭目以待！', '世界杯', '0', '2018-07-12 19:45:11', '../img/blog/1531395911095soccer.jpg', '1');
INSERT INTO `post` VALUES ('29', 'Richard', 'Wonderful', 'Wonderful world, onderful goal!', 'Wonderful world!', '33', '2018-07-12 19:51:44', '../img/blog/1531396304194blog5.jpg', '1');
INSERT INTO `post` VALUES ('30', 'zss', 'wadas', 'asdasdasd', 'saddas', '0', '2018-07-13 08:25:57', '../img/blog/15314415573831531395231323i-phone.jpg', '0');
INSERT INTO `post` VALUES ('31', 'admin', 'asdas', 'asdas', 'asdasd', '0', '2018-07-13 08:28:04', '../img/blog/15314416848421531395231323i-phone.jpg', '0');
INSERT INTO `post` VALUES ('32', 'zss', 'asdas', 'asdasdas', 'aasdas', '0', '2018-07-13 08:42:55', '../img/blog/15314425751601531395231323i-phone.jpg', '1');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `productID` int(15) NOT NULL AUTO_INCREMENT,
  `productname` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `brand` varchar(10) DEFAULT NULL,
  `price` float(10,2) NOT NULL,
  `quantity` int(5) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `lensimg` varchar(200) NOT NULL,
  `detail` varchar(200) NOT NULL,
  `saleVolume` int(8) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('40', 'HUAWEI P20', '限量送精美配件', 'MobilePhone', 'HUAWEI', '3799.00', '20', 'img/products/indexImage/p2.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', '白色', '大');
INSERT INTO `product` VALUES ('6', '荣耀10', '增148配件好礼，享499元美食卡', 'MobilePhone', 'HUAWEI华为', '2599.00', '30', 'img/products/indexImage/p1.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'purple', '5英寸全面屏');
INSERT INTO `product` VALUES ('7', 'HUAWEI HONOR v10', '最高优惠500', 'MobilePhone', 'HUAWEI华为', '2555.00', '50', 'img/products/indexImage/p3.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'red', '5英寸全面屏');
INSERT INTO `product` VALUES ('8', 'HUAWEI MATE 10 PRO', '限时优惠500', 'MobilePhone', 'HUAWEI华为', '3500.00', '30', 'img/products/indexImage/p4.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'black', '5英寸全面屏');
INSERT INTO `product` VALUES ('9', 'iphone X', '限时发售', 'MobilePhone', 'iphone苹果', '9999.00', '50', 'img/products/indexImage/p5.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'white', '5英寸全面屏');
INSERT INTO `product` VALUES ('10', 'HUAWEI HONOR7X', '官方旗舰店正品发售', 'MobilePhone', 'HUAWEI华为', '3500.00', '50', 'img/products/indexImage/p6.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'red', '5英寸全面屏');
INSERT INTO `product` VALUES ('11', 'OPPO R11', '全面屏拍照美颜正品手机', 'MobilePhone', 'OPPO', '2699.00', '100', 'img/products/indexImage/p7.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'pink', '5英寸全面屏');
INSERT INTO `product` VALUES ('12', 'HUAWEI HONOR7C', '华为7C全面屏正品手机', 'MobilePhone', 'HUAWEI华为', '2599.00', '100', 'img/products/indexImage/p8.jpg', 'img/products/1531444732689.jpg|img/products/1531388687736.jpg|img/products/1531402095641.jpg|img/products/1531426151724.jpg|', 'img/products/1531452374274.jpg', '0', 'blue', '5英寸全面屏');
INSERT INTO `product` VALUES ('13', '神舟 战神Z7-KPG', '天猫直销', 'Laptop', 'HASEE神舟', '7088.00', '50', 'img/products/indexImage/L5.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '14英寸大屏电脑');
INSERT INTO `product` VALUES ('14', 'Daysky DK 190', '轻薄学生14英寸手提办公商务游戏本', 'Laptop', 'DAYSKY', '1298.00', '100', 'img/products/indexImage/L9.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '14英寸大屏电脑');
INSERT INTO `product` VALUES ('15', 'MacBook Air', '笔记本电脑13.3英寸金属轻薄便携', 'Laptop', 'Apple', '9999.00', '100', 'img/products/apple1.jpg', 'img/products/apple_lens1.jpg|img/products/apple_lens2.jpg|img/products/apple_lens4.jpg|img/products/apple_lens3.jpg|', 'img/products/apple_desc.jpg', '0', '白色', '大');
INSERT INTO `product` VALUES ('16', '拯救者 Y7000', '联想15.6英寸游戏本笔记本', 'Laptop', 'Lenovo联想', '7599.00', '200', 'img/products/indexImage/L6.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '15.6英寸');
INSERT INTO `product` VALUES ('17', 'DELL 游戏本', '吃鸡利器，GTX1050', 'Laptop', 'DELL戴尔', '5999.00', '50', 'img/products/indexImage/L8.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '15.6英寸大屏');
INSERT INTO `product` VALUES ('18', 'DMC-LX10GK-K', 'DMC-LX10GK-K数码高清照相机', 'Camera', 'Panasonic松', '3698.00', '50', 'img/products/indexImage/C1.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '中小');
INSERT INTO `product` VALUES ('19', '佳能80D 19-200 18-135', '单机专业单反数码相机', 'Camera', 'Canon佳能', '5799.00', '50', 'img/products/indexImage/C7.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '中小');
INSERT INTO `product` VALUES ('20', '富士 X100f旁轴数码相机', '文艺复古X100F', 'Camera', 'Fujifilm富士', '8690.00', '50', 'img/products/indexImage/C4.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '小型');
INSERT INTO `product` VALUES ('21', 'Canon PowerShot G7 X Mark Ⅱ', '旗舰店直销', 'Camera', 'Canon佳能', '3999.00', '100', 'img/products/indexImage/C3.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '中小');
INSERT INTO `product` VALUES ('22', '佳能EOS 77D', '单反相机数码相机套机 保证正品', 'Camera', 'Canon佳能', '6538.00', '100', 'img/products/indexImage/C2.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '中小型');
INSERT INTO `product` VALUES ('23', 'Apple iPad Pro 平板电脑', '256G WLAN版 A10X芯片', 'Tablet', 'APPLE苹果', '5788.00', '50', 'img/products/indexImage/T1.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'black', '10.5英寸');
INSERT INTO `product` VALUES ('24', 'Apple iPad 平板电脑', '2018年新款9.7英寸', 'Tablet', 'APPLE苹果', '2488.00', '100', 'img/products/indexImage/T2.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'gold', '9.7英寸');
INSERT INTO `product` VALUES ('25', '微软新Surface Pro', '二合一平板电脑', 'Tablet', 'Microsoft微', '8288.00', '100', 'img/products/indexImage/T3.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '12.3英寸');
INSERT INTO `product` VALUES ('26', 'Apple iPad mini4', '平板电脑7.9英寸 金色 128G WLAN版', 'Tablet', 'APPLE苹果', '2619.00', '100', 'img/products/indexImage/T4.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'gole', '7.9英寸');
INSERT INTO `product` VALUES ('27', '华为HUAWEI M3', '青春版 10.1英寸平板电脑', 'Tablet', 'HUAWEI华为', '2099.00', '100', 'img/products/indexImage/T5.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', 'white', '10.1英寸');
INSERT INTO `product` VALUES ('38', 'Apple air', '苹果专业办公笔记本、办公利器', 'Laptop', 'Apple', '9999.00', '456', 'img/products/1532082360343.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532085178116.jpg', '0', '白色', '中');
INSERT INTO `product` VALUES ('39', 'HUAWEI BOOK', '华为白领笔记本', 'Laptop', 'HUAWEI', '7800.00', '30', 'img/products/1532001348389.jpg', 'img/products/1532018510516.jpg|img/products/1532032212969.jpg|img/products/1532020700002.jpg|img/products/1532033456563.jpg|', 'img/products/1532025252470.jpg', '0', '白色', '大');
INSERT INTO `product` VALUES ('42', 'Apple笔记本电脑', '办公专属，十分可以', 'Laptop', 'Apple', '9999.00', '300', 'img/products/1531393346877.jpg', 'img/products/1531385720396.jpg|img/products/1531374004176.jpg|img/products/1531384118526.jpg|img/products/1531352106576.jpg|', 'img/products/1531349178529.jpg', '0', '白色', '大');
INSERT INTO `product` VALUES ('43', '华为 Matebook X WT-W09 i5', '【下单选择礼盒套餐更优惠】配扩展坞和office软件，2K超高清屏，固态SSD疾速启动，指纹开机移动办公更便捷', null, null, '5988.00', '70', 'img/products/huawei1.jpg', 'huawei_lens1.jpg|huawei_lens2.jpg|huawei_lens3.jpg|huawei_lens4.jpg', 'img/seckill/huawei_desc.jpg', null, '钛银灰|香槟金', '8GB+256GB|4Gb+256GB');
INSERT INTO `product` VALUES ('44', 'Apple MacBook Air', '苹果（Apple） MacBook Air 苹果笔记本电脑 13.3英寸 套餐版下单即送大礼包 2017款/i5/8GB/128GB/D32', null, null, '5788.00', '100', 'img/products/apple1.jpg', 'apple_lens1.jpg|apple_lens2.jpg|apple_lens3.jpg|apple_lens4.jpg', 'img/seckill/apple_desc.jpg', null, '钛银灰', '8GB+128GB|8Gb+256GB');
INSERT INTO `product` VALUES ('45', 'Dell戴尔 xps13 i5', '戴尔笔记本暑期好价冰凉来袭！限时抢购，手慢无哦~', null, null, '7199.00', '100', 'img/products/dell1.jpg', 'dell_lens1.jpg|dell_lens2.jpg|dell_lens3.jpg|dell_lens4.jpg', 'img/seckill/dell_desc.jpg', null, '银色|金色', '16GB 512GB|8GB 256GB');
INSERT INTO `product` VALUES ('46', 'OPPO FindX', '8+128G 曲面全景屏一体机身 全隐藏式摄像头', null, null, '4999.00', '10', 'img/products/oppo1.jpg', 'oppo_lens2.jpg|oppo_lens3.jpg|oppo_lens6.jpg|oppo_lens7.jpg', 'img/seckill/oppo_desc.jpg', null, '波尔多红|冰珀蓝', '官方标配8+128GB');
INSERT INTO `product` VALUES ('47', '美图T8s', '【12期免息】Meitu/美图 T8s 美图t8s自拍美颜手机现货4G拍照手机\\r\\n 升级套餐 闪送当日达', null, null, '3299.00', '90', 'img/products/meitu1.jpg', 'oppo_lens2.jpg|oppo_lens3.jpg|oppo_lens6.jpg|oppo_lens7.jpg', 'img/seckill/oppo_desc.jpg', null, '莫奈粉|暗夜蓝|冰川蓝|烈焰蓝', '官方标配128GB');
INSERT INTO `product` VALUES ('48', 'Xiaomi小米 8', 'Xiaomi/小米 小米8年度旗舰全面屏骁龙845双频GPS智能拍照手机 4G+全网通', null, null, '3199.00', '70', 'img/products/xiaomi1.jpg', 'xiaomi_lens1.jpg|xiaomi_lens2.jpg|xiaomi_lens3.jpg|xiaomi_lens4.jpg', 'img/seckill/xiaomi_desc.jpg', null, '黑色|白色|金色|蓝色', '6+128GB|6+256GB');
INSERT INTO `product` VALUES ('49', '1', '1', 'Laptop', 'HUAWEI', '6000.00', '300', 'img/products/1531455262679.jpg', 'img/products/1531496230535.jpg|img/products/1531520965244.jpg|img/products/1531504472047.jpg|img/products/1531499917743.jpg|', 'img/products/1531492969605.jpg', '0', '白色', '大');

-- ----------------------------
-- Table structure for `productcomment`
-- ----------------------------
DROP TABLE IF EXISTS `productcomment`;
CREATE TABLE `productcomment` (
  `productcomID` int(11) NOT NULL AUTO_INCREMENT,
  `evaluategrade` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `productcom` varchar(200) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`productcomID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of productcomment
-- ----------------------------
INSERT INTO `productcomment` VALUES ('1', '比较好', 'zss', 'dengjieasdf', '2018-07-12 17:32:12');
INSERT INTO `productcomment` VALUES ('5', '比较好', 'zss', 'sdasd', '2018-07-13 08:08:15');
INSERT INTO `productcomment` VALUES ('3', '比较好', 'zss', '你好！！！', '2018-07-12 20:09:22');
INSERT INTO `productcomment` VALUES ('6', '比较好', 'zss', 'asdas', '2018-07-13 10:56:57');

-- ----------------------------
-- Table structure for `seckill`
-- ----------------------------
DROP TABLE IF EXISTS `seckill`;
CREATE TABLE `seckill` (
  `seckillID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(15) NOT NULL,
  `productname` varchar(50) DEFAULT NULL,
  `seckill_starttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seckill_endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sec_para` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`seckillID`),
  KEY `productID` (`productID`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seckill
-- ----------------------------
INSERT INTO `seckill` VALUES ('1', '43', '华为Matebook X WT-W09 i5', '2018-07-13 08:14:12', '2018-07-14 00:00:00', 'huawei_parameter.jpg');
INSERT INTO `seckill` VALUES ('2', '44', 'Apple MacBook Air', '2018-07-08 09:18:48', '2018-07-08 09:51:06', 'apple_parameter.jpg');
INSERT INTO `seckill` VALUES ('4', '45', 'Dell戴尔 xps13 i5', '2018-07-12 09:18:57', '2018-07-08 08:16:57', 'dell_parameter.jpg');
INSERT INTO `seckill` VALUES ('8', '47', '美图T8s', '2018-07-12 09:19:15', '2018-07-14 17:02:20', 'meitu_parameter.jpg');
INSERT INTO `seckill` VALUES ('7', '46', 'OPPO FindX', '2018-07-12 09:19:06', '2018-07-14 15:41:02', 'oppo_parameter.jpg');
INSERT INTO `seckill` VALUES ('9', '48', 'Xiaomi小米 8', '2018-07-12 09:19:23', '2018-07-10 16:14:32', 'xiaomi_parameter.jpg');

-- ----------------------------
-- Table structure for `success_kill`
-- ----------------------------
DROP TABLE IF EXISTS `success_kill`;
CREATE TABLE `success_kill` (
  `seckillID` int(11) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`seckillID`,`tel`),
  KEY `tel` (`tel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of success_kill
-- ----------------------------
INSERT INTO `success_kill` VALUES ('0', '0', '1');
INSERT INTO `success_kill` VALUES ('2', '12354566', '0');
INSERT INTO `success_kill` VALUES ('1', '34278', '0');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userID` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `money` int(10) DEFAULT NULL,
  `score` int(8) DEFAULT NULL,
  `registTime` date NOT NULL,
  `usertype` varchar(10) NOT NULL,
  `state` int(5) NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('14', 'admin', '123456', '0', '0', '2018-07-19', 'admin', '1');
INSERT INTO `user` VALUES ('25', '18821635187', '123456', '0', '0', '2018-07-11', 'user', '1');
INSERT INTO `user` VALUES ('18', '18821715048', '123456', '1223', '0', '2018-07-14', 'user', '1');
INSERT INTO `user` VALUES ('13', '18276211049', '123456', '0', '0', '2018-07-28', 'user', '0');
INSERT INTO `user` VALUES ('17', '15047835905', '123456', '0', '0', '2018-07-21', 'user', '1');
INSERT INTO `user` VALUES ('20', 'admin2', '123456', '0', '0', '2018-07-14', 'user', '1');
INSERT INTO `user` VALUES ('21', 'admin3', '1234567', '0', '0', '2018-07-14', 'admin', '1');
INSERT INTO `user` VALUES ('22', 'super', '123456', '0', '0', '2018-07-12', 'sAdmin', '1');
INSERT INTO `user` VALUES ('31', '15229315883', '123456', '0', '0', '2018-07-13', 'user', '1');
INSERT INTO `user` VALUES ('30', 'zss', '123456', '0', '0', '2018-07-19', 'user', '1');
INSERT INTO `user` VALUES ('1', 'Richard', '123456', '0', '0', '2018-07-17', 'user', '1');
INSERT INTO `user` VALUES ('2', 'su', '123456', '0', '0', '2018-07-12', 'user', '1');
INSERT INTO `user` VALUES ('3', 'wl', '123456', '0', '0', '2018-07-12', 'user', '1');
INSERT INTO `user` VALUES ('32', '18821701702', '123456', '0', '0', '2018-07-13', 'user', '1');
INSERT INTO `user` VALUES ('34', '18821702370', '123456', '0', '0', '2018-07-13', 'user', '1');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `userInfoID` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `truename` varchar(50) DEFAULT NULL,
  `gender` varchar(5) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userInfoID`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', 'Richard', 'img/blog/b5.jpg', 'Tc', '1234456', '陶聪', '男', '2018-07-12', '');
INSERT INTO `userinfo` VALUES ('2', '2015012784', null, 'Tc', '1234456', '陶聪', '男', '2018-06-04', null);
INSERT INTO `userinfo` VALUES ('3', 'su', 'img/blog/b7.jpg', 'susu', '18821715048', '苏祥瑞', '男', '2018-07-12', '18821715048@qq.com');
INSERT INTO `userinfo` VALUES ('4', 'zss', 'img/blog/dell1.jpg', 'tczwasds', '18821701702', '涛从', '男', '2018-07-13', '1821701702@163.com');
INSERT INTO `userinfo` VALUES ('5', 'Bill', null, '', '', '', '', '2018-06-28', null);
INSERT INTO `userinfo` VALUES ('6', 'sususu', null, '', '', '', '', '2018-06-28', null);
INSERT INTO `userinfo` VALUES ('7', 'susu', null, '', '', '', '', '2018-06-28', null);
INSERT INTO `userinfo` VALUES ('8', '2034', null, '', '', '', '', '2018-06-28', null);
INSERT INTO `userinfo` VALUES ('9', 'wl', 'img/blog/b8.jpg', '王司徒', '18934346345', '王朗', '男', '2018-07-12', '18934346345@qq.com');
INSERT INTO `userinfo` VALUES ('14', '17791389021', null, '', '', '', '', '2018-07-12', null);
INSERT INTO `userinfo` VALUES ('11', '18821635187', 'img/blog/apple_desc.jpg', 'TCL', '18821715048', '涛从', '男', '2018-07-12', '942045427@qq.com');
INSERT INTO `userinfo` VALUES ('15', '17791389021', null, '', '', '', '', '2018-07-12', null);
INSERT INTO `userinfo` VALUES ('16', '15229315883', null, '', '', '', '', '2018-07-13', null);
INSERT INTO `userinfo` VALUES ('17', '18821701702', null, '', '', '', '', '2018-07-13', null);
INSERT INTO `userinfo` VALUES ('18', '18821702370', null, '', '', '', '', '2018-07-13', null);
INSERT INTO `userinfo` VALUES ('19', '18821702370', null, '', '', '', '', '2018-07-13', null);
