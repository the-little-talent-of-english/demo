package com.tedu.store.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tedu.store.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.tedu.store.service.MallService;
import com.tedu.store.Util.DateUtil;

@Controller
@RequestMapping("/mall")
@CrossOrigin // 跨域支持
public class MallController {
    @Autowired
    private MallService mallService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> hello(HttpSession session, HttpServletRequest request) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg", mallService.hello());
        return map;
    }

    @RequestMapping(value = "/product-list", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> productIDList(
            HttpSession session,
            HttpServletRequest request,
            @RequestBody Map<String, Object> req
    ) throws Exception {
        String itemType = (String) (req.get("item_type"));
        if (itemType.equals("book")) {
            List<Integer> ids = mallService.getAllBookIDs();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ids", ids);
            return map;
        }
        // TODO 其他类型商品
        return null;
    }


    @RequestMapping(value = "/product-summary", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> productSummary(
            HttpSession session,
            HttpServletRequest request,
            @RequestBody Map<String, Object> req
    ) throws Exception {
        String itemType = (String) (req.get("item_type"));
        if (itemType.equals("book")) {
            int id;
            Object rawID = req.get("id");
            if (rawID instanceof String) {
                id = Integer.parseInt((String) rawID);
            } else {
                id = (Integer) (req.get("id"));
            }
            Book book = mallService.getBookSummaryById(id);
            Map<String, Object> res = bookToSummary(book);
            return res;
        }
        // TODO 其他类型商品
        return null;
    }

    @RequestMapping(value = "/product-details", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> productDetails(
            HttpSession session,
            HttpServletRequest request,
            @RequestBody Map<String, Object> req
    ) throws Exception {
        String itemType = (String) (req.get("item_type"));
        if (itemType.equals("book")) {
            int id;
            Object rawID = req.get("id");
            if (rawID instanceof String) {
                id = Integer.parseInt((String) rawID);
            } else {
                id = (Integer) (req.get("id"));
            }
            Book book = mallService.getBookDetailsById(id);
            Map<String, Object> res = bookToDetails(book);
            return res;
        }
        // TODO 其他类型商品
        return null;
    }


    private static Map<String, Object> bookToSummary(Book book) {
        String timeStr = DateUtil.gettime();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("itemType", "book");
        map.put("itemID", book.getID());
        map.put("name", book.getName());
        map.put("points", book.getPrice());
        map.put("date", timeStr);
        map.put("imgUrl", book.getImgUrl());
        return map;

    }

    private static Map<String, Object> bookToDetails(Book book) {
        Map<String, Object> map = MallController.bookToSummary(book);
        String basicInfo = book.getInfo();
        String bookIntro = book.getBookIntro();
        String authorIntro = book.getAuthorIntro();
        String description = "<div>" + basicInfo + "</div>" +
                "<div>" +
                "<h3>内容简介</h3>" +
                "<div>" + bookIntro + "</div>" +
                "</div>" +
                "<div>" +
                "<h3>作者简介</h3>" +
                "<div>" + authorIntro + "</div>" +
                "</div>";
        map.put("description", description);
        return map;
    }

}
