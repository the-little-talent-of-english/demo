package com.tedu.store.mapper;

import java.util.List;
import com.tedu.store.entity.Book;

public interface BookMapper {
    List<Integer> getAllBookIDs();
    Book getBookSummaryById(int bookID);
    Book getBookDetailsById(int bookID);
}
