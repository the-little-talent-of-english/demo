package com.tedu.store.service;


import com.tedu.store.entity.Book;

import java.util.List;

public interface MallService {
	public String hello();
	List<Integer> getAllBookIDs();
	Book getBookSummaryById(int bookID);
	Book getBookDetailsById(int bookID);
}
