package com.tedu.store.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.tedu.store.entity.Book;
import com.tedu.store.mapper.BookMapper;
import com.tedu.store.service.MallService;

@Service
public class MallServiceImpl implements MallService {

    @Autowired
    private BookMapper bookMapper;

    public String hello() {
        return "hello world";
    }

    public List<Integer> getAllBookIDs() {
        return bookMapper.getAllBookIDs();
    }

    public Book getBookSummaryById(int bookID) {
        return bookMapper.getBookSummaryById(bookID);
    }

    public Book getBookDetailsById(int bookID) {
        return bookMapper.getBookDetailsById(bookID);
    }
}
