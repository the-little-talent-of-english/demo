﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<meta name='TTUNION_verify' content='b846c3c2b85efabc496d2a2b8399cd62'>
	<meta name="sogou_site_verification" content="gI1bINaJcL"/>
	<meta name="360-site-verification" content="37ae9186443cc6e270d8a52943cd3c5a" />
	<meta name="baidu_union_verify" content="99203948fbfbb64534dbe0f030cbe817">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">

	<title>登录页面</title>

	<link rel="stylesheet" type="text/css" href="./css/base.css">
	<link rel="stylesheet" type="text/css" href="./css/home.css">
	<link rel="stylesheet" href="./lib/layui/css/layui.css"  media="all">
	<script type="text/javascript"
		src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
	<script src="./lib/layui/layui.js" charset="utf-8"></script>
	<script type="text/javascript" src="./js/xadmin.js"></script>
	<script src="laydate/laydate.js"></script>
	<script type="text/javascript">
	document.getElementById("changeImg").onclick = function(){
	 document.getElementById("changeImg").src="./user/createImage.do";
	}
	$('#changeImg').click(function(){
		  $('#codeView').load('./user/createImage.do');
		  });
    function getkey(a) {
        var pms = 'tel=' + document.getElementById("tel").value;
  		a.href = './user/sendMessage.do?' + pms;
  }
  

</script>
</head>
<body>

	<header class="aui-header">
		<div class="aui-header-box">
			<h1>
				<a href="index.jsp" class="aui-header-box-logo"></a>
			</h1>			
		</div>
	</header>

	<section class="aui-content">
		<div class="aui-content-box clearfix">
			<div class="aui-content-box-fl">
				<div class="aui-form-header">
					<div class="aui-form-header-item on">密码登录</div>
					<div class="aui-form-header-item">手机登录</div>
					<span class="aui-form-header-san"></span>
				</div>
				<div class="aui-form-content">
					<div class="aui-form-content-item">
					<span id="alertMsg"  style="color:red;margin-left:35%;width:300px;text-align:center;">${alertMsg}</span>
						<form action="./user/login.do" method="post">
							<div class="aui-form-list">
								<input type="text" class="aui-input" id="username" onblur="checkTel()" name="username" placeholder="请输入手机号/邮箱" data-required="required" autocomplete="off">
							</div>
							<div class="aui-form-list">
								<input type="password" class="aui-input" id="password" name="password" onblur="checkPassword()"placeholder="请输入密码" data-required="required" autocomplete="off">
							</div>
							<div class="aui-form-list" style="float:left;width:170px;margin-bottom:5px;">
								<input type="text" class="aui-input" id="code" onblur="checkCode()"name="code" placeholder="请输入验证码" data-required="required" autocomplete="off">								
							</div>
							<iframe src="./user/createImage.do" name="codeView" width="145px" height="42px" align="bottom" frameborder="0">
  							</iframe>
							
							<div class="aui-form-pwd clearfix" style="height:7px">
								<div style="float:left;htight:1px;">
									<a href="./updatePassword.jsp">忘记密码？</a>
								</div>
								<div  style="float:right;htight:1px;">
									<a href="./user/createImage.do" target="codeView">看不清，换一张</a>
								</div>
							</div>
							<div class="aui-form-btn">
								<input type="hidden" name="usertype" value="user"/>
								<input type="submit"  class="aui-btn" value="登&nbsp;录" >
							</div>
						</form>
					</div>
					<div class="aui-form-content-item">
						<form action="./user/telLogin.do" id="telForm">
							<div class="aui-form-list">
								<input type="text" class="aui-input" id="tel" name="tel" placeholder="请输入手机号" data-required="required" autocomplete="off">
							</div>
							<div class="aui-form-list">
								<input type="text" class="aui-input" name="tcode" placeholder="请输入手机验证码" data-required="required" autocomplete="off">
								<input type="hidden" name="usertype" value="user"/>
								<a href="" onclick="getkey(this)" target="_blank"><input type="button" class="aui-child" value="获取验证码"/></a>
							</div>
							<div class="aui-form-pwd clearfix" style="height:7px">
									<a href="./updatePassword.jsp">忘记密码？</a>
							</div>
							<div class="aui-form-btn">
								<input type="submit" class="aui-btn" value="登&nbsp;录">
								
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="aui-content-box-fr">
				<div class="aui-content-box-text">
					<h3>还没有帐号:</h3>
					<a href="/D-Star/regist.jsp" class="aui-ll-link">立即注册</a>
					<h3>您是管理员:</h3>
					<a href="/D-Star/backLogin.jsp" class="aui-ll-link">后台登录</a>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript">
        $(function(){

			/*tab标签切换*/
            function tabs(tabTit,on,tabCon){
                $(tabCon).each(function(){
                    $(this).children().eq(0).show();

                });
                $(tabTit).each(function(){
                    $(this).children().eq(0).addClass(on);
                });
                $(tabTit).children().click(function(){
                    $(this).addClass(on).siblings().removeClass(on);
                    var index = $(tabTit).children().index(this);
                    $(tabCon).children().eq(index).show().siblings().hide();
                });
            }
            tabs(".aui-form-header","on",".aui-form-content");

        })
	</script>
	<%session.removeAttribute("alertMsg"); %>
</body>
</html>