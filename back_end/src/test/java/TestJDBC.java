
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 测试连接
 */
public class TestJDBC {
    public static void main(String[] args) {
        //加载驱动类
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Class.forName("com.mysql.cj.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/englishda?useUnicode=true&characterEncoding=utf-8";
            Connection conn = DriverManager.getConnection(url, "root", "123456");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}