#!/usr/bin/bash

# 将mvn生成的war文件移动到 target/__tomcat__ 中
# __tomcat__ 可挂载到 tomcat 的 docker 容器中
# docker run -p 58080:8080 -v $WAR_PATH:/usr/local/tomcat/webapps/ tomcat

COMMON_DIR=$(dirname $0)
export BASE_DIR=$(cd ${COMMON_DIR};cd ..;pwd)
export TARGET_DIR="${BASE_DIR}/target/"
export TOMCAT_DIR="__tomcat__"
export WAR_FILE="SSM-0.0.1-SNAPSHOT.war"

if [ ! -d "${TARGET_DIR}/${TOMCAT_DIR}" ]; then
    mkdir "${TARGET_DIR}/${TOMCAT_DIR}"
fi

if [ -f "${TARGET_DIR}/${WAR_FILE}" ]; then
    mv "${TARGET_DIR}/${WAR_FILE}" "${TARGET_DIR}/${TOMCAT_DIR}/${WAR_FILE}"
fi