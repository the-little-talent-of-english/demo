package com.tedu.store.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tedu.store.entity.User;

/**
 * Servlet Filter implementation class SAdminFilter
 */
public class SAdminFilter implements Filter {

    /**
     * Default constructor. 
     */
    public SAdminFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest) request;
		HttpServletResponse res=(HttpServletResponse) response;
		HttpSession session=req.getSession();
		if ((User) session.getAttribute("user")==null){
			session.setAttribute("alertMsg", "*您还没有登录*");
			res.sendRedirect("../login.jsp");
			return;
		}
		User user=(User) session.getAttribute("user");
		if(user.getUsertype().equals("sAdmin")==false){
			session.setAttribute("alertMsg", "*您没有权限访问*");
			res.sendRedirect("../backLogin.jsp");
			return;
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
