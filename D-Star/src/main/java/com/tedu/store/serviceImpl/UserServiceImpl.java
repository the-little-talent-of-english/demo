package com.tedu.store.serviceImpl;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tedu.store.entity.User;
import com.tedu.store.mapper.UserMapper;
import com.tedu.store.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserMapper userMapper;

	
	public boolean insertUser(User user) {
		if (userMapper.getUserByName(user.getUsername())==null){
			userMapper.insertUser(user);

			return true;
		}
		return false;
	}


	public boolean validate(User user) {
		if (userMapper.getUserByName(user.getUsername())==null){
			return false;
		}else{
			if (userMapper.getUserByName(user.getUsername()).getPassword().equals(user.getPassword())&&userMapper.getUserByName(user.getUsername()).getUsertype().equals(user.getUsertype())){
				return true;
			}
		}
		return false;
	}


	public void updatePassword(User user) {
		userMapper.updatePassword(user);
	}


	public User getUserByName(String username) {
		User user=userMapper.getUserByName(username);
		return user;
	}
	
	public User getUserById(int userID) {
		User user=userMapper.getUserById(userID);
		return user;
	}



	public List<User> getAllUsers() {
		List<User> users=userMapper.getUsers();
		return users;
	}


	public void modifyUser(User user) {
		userMapper.modifyUser(user);
	}


	public void deleteUser(int userID) {
		userMapper.deleteUser(userID);
	}


	public List<User> getAllSysUsers() {
		List<User> sysUsers=userMapper.getAllSysUsers();
		return sysUsers;
	}
	
	
}
