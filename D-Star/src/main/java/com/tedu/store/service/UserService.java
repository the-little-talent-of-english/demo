package com.tedu.store.service;


import java.util.List;

import com.tedu.store.entity.User;

public interface UserService {
	public boolean insertUser(User user);
	public boolean validate(User user);
	public void updatePassword(User user);
	public User getUserByName(String username);
	public List<User> getAllUsers();
	public User getUserById(int userID);
	public void modifyUser(User user);
	public void deleteUser(int userID);
	public List<User> getAllSysUsers();
}
