package com.tedu.store.mapper;

import java.util.List;

import com.tedu.store.entity.User;

public interface UserMapper {
	public User getUserByName(String username);
	public User getUserById(int userID);
	public void insertUser(User user);
	public void updatePassword(User user);
	public List<User> getUsers();
	public void modifyUser(User user);
	public void deleteUser(int userID);
	public List<User> getAllSysUsers();
}
