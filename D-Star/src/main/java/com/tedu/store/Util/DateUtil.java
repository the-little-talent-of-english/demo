package com.tedu.store.Util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public Long getdate(){
		Date d=new Date();//获取时间
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");//转换格式
		//System.out.println(sdf.format(d));
		Long orderID=Long.parseLong(sdf.format(d).trim());
		System.out.println(orderID);
		return orderID;
	}
	
	public String gettime(){
		Date d=new Date();//获取时间
		String time = d.toString();
		System.out.println(time);
		return time;
	}
}
