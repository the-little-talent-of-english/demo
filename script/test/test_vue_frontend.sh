cd "${WORKPLACE}/front_end/"

npm run test:unit

# 检查结束代码(exit_code)是否为 0
_exit_code=$?

# 将测试结果拷贝到指定目录
target_dir="${TEST_REPORTS_DIR}/vue/"
if [ ! -d "${target_dir}" ]; then
    mkdir ${target_dir}
fi
cp ${WORKPLACE}/front_end/test_reports/*.xml "${target_dir}"

# exit_code 检查
if [ $_exit_code -eq 0 ]; then
    echo "over"
else
    echo "failed"
    exit $_exit_code
fi