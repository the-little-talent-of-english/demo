echo "[deprecated: 2021-06-09 23:30:33]
mvn clean package 时会自动测试，不需要额外写测试脚本"

# 检查结束代码(exit_code)是否为 0
_exit_code=$?

# 将测试结果拷贝到指定目录
target_dir="${TEST_REPORTS_DIR}/springboot/"
if [ ! -d "${target_dir}" ]; then
    mkdir ${target_dir}
fi
cp ${WORKPLACE}/back_end2/target/surefire-reports/*.xml "${target_dir}"

# exit_code 检查
if [ $_exit_code -eq 0 ]; then
    echo "over"
else
    echo "failed"
    exit $_exit_code
fi