export SCRIPT_DIR=$(cd $(dirname $0);pwd)
export WORKPLACE=$(cd "${SCRIPT_DIR}/..";pwd)
export TEST_REPORTS_DIR="${WORKPLACE}/test_reports"

if [ ! -d "${TEST_REPORTS_DIR}" ]; then
    mkdir "${TEST_REPORTS_DIR}"
fi

echo "=========================================================="
echo "tests start: $(date)"
echo "\$WORKPLACE: ${WORKPLACE}"
echo "=========================================================="
echo -e "\n\n"

exit_code=0

for i in ${SCRIPT_DIR}/test/*.sh; do
    if [ -r "$i" ]; then 
        echo "----------------------------------------------------------"
        echo $i
        echo "----------------------------------------------------------"
        sh "$i"
        exit_code=$?
        echo "exit code: ${exit_code}"
        echo -e "\n"
    fi
    if [ ! $exit_code -eq 0 ]; then
        break
    fi
done

tree "${TEST_REPORTS_DIR}"

echo -e "\n\n"
echo "=========================================================="
echo "tests end: $(date)"
echo "exit code: ${exit_code}"
echo "=========================================================="
exit ${exit_code}
