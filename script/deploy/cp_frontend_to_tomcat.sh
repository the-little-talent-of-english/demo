cd "${WORKPLACE}/front_end/"

# _target="/usr/local/tomcat/webapps/front-end"
# if [ -d "${_target}" ]; then
#     rm -r "${_target}"
# fi
# cp -r "dist" "${_target}"

echo "[deprecated: 2021-06-07 22:41:09]
vue编译出来的前端程序似乎不能以直接放到webapps目录的方式交由tomcat运行，
还不如直接使用 python3 -m http.server 8888"

# 检查结束代码(exit_code)是否为 0
_exit_code=$?
if [ $_exit_code -eq 0 ]; then
    echo "over"
else
    echo "failed"
    exit $_exit_code
fi