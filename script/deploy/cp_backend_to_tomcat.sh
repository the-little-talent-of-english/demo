cd "${WORKPLACE}/back_end2/"
cp "target/demo-0.0.1-SNAPSHOT.war" "/usr/local/tomcat/webapps/back-end.war"

# 检查结束代码(exit_code)是否为 0
_exit_code=$?
if [ $_exit_code -eq 0 ]; then
    echo "over"
else
    echo "failed"
    exit $_exit_code
fi