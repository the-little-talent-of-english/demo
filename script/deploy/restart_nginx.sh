cd "${SCRIPT_DIR}/run/nginx"

container_name="englishda-nginx"

if [ -n "$(docker ps | grep ${container_name})" ]; then
    docker stop "${container_name}"
fi

bash docker-run-nginx.sh

# 检查结束代码(exit_code)是否为 0
_exit_code=$?
if [ $_exit_code -eq 0 ]; then
    echo "over"
else
    echo "failed"
    exit $_exit_code
fi