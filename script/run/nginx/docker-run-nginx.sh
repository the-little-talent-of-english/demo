# docker run --rm -it --name=englishda-nginx -v ${PWD}/nginx-conf:/etc/nginx/conf.d -p 12345:80 -p 18080:18080 nginx


export SCRIPT_DIR=$(cd $(dirname $0);cd ..;cd ..;pwd)
export WORKPLACE=$(cd "${SCRIPT_DIR}/..";pwd)

FRONT_END_DIR="${WORKPLACE}/front_end/dist"

if [ -d "${FRONT_END_DIR}" ]; then
    echo ${FRONT_END_DIR}
else
    echo "front_end directory '${${FRONT_END_DIR}}' doesn't exist."
    exit 1
fi

# 后台运行
docker run --rm -d --name=englishda-nginx \
    -v ${PWD}/nginx-conf:/etc/nginx/conf.d \
    -v ${PWD}/nginx.conf:/etc/nginx/nginx.conf \
    -v ${FRONT_END_DIR}:/front-end/ \
    -p 12345:80 \
    -p 18080:18080 \
    nginx