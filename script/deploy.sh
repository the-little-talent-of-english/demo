export SCRIPT_DIR=$(cd $(dirname $0);pwd)
export WORKPLACE=$(cd "${SCRIPT_DIR}/..";pwd)

echo "=========================================================="
echo "deployment starts: $(date)"
echo "\$WORKPLACE: ${WORKPLACE}"
echo "=========================================================="
echo -e "\n\n"

exit_code=0

for i in ${SCRIPT_DIR}/deploy/*.sh; do
    if [ -r "$i" ]; then 
        echo "----------------------------------------------------------"
        echo $i
        echo "----------------------------------------------------------"
        sh "$i"
        exit_code=$?
        echo "exit code: ${exit_code}"
        echo -e "\n"
    fi
    if [ ! $exit_code -eq 0 ]; then
        break
    fi
done

echo -e "\n\n"
echo "=========================================================="
echo "deployment ends: $(date)"
echo "exit code: ${exit_code}"
echo "=========================================================="
exit ${exit_code}
