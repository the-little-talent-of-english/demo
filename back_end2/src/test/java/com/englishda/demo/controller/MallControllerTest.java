package com.englishda.demo.controller;

import com.englishda.demo.service.MallService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.junit.platform.commons.logging.Logger;
//import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;

import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class MallControllerTest {


    private static final Logger logger = LoggerFactory.getLogger(MallController.class);

    @Autowired
    private MallController mallController;

    private MockMvc mockMvc;


    private MockHttpSession session;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(mallController).build();
        session = new MockHttpSession();
    }

    @Test
    void hello() throws Exception {
        MvcResult mvcResult = (MvcResult) mockMvc.perform(
                MockMvcRequestBuilders.get("/mall/hello")
                        .accept(MediaType.ALL)
                        .session(session)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //得到返回代码
        int status = mvcResult.getResponse().getStatus();
        //得到返回结果
        String content = mvcResult.getResponse().getContentAsString();
        logger.info(content);
    }

    @Test
    void productIDList() throws Exception {
        Map map = new HashMap();
        map.put("item_type", "book");
        String reqString = MallControllerTest.mapToString(map);
        MvcResult mvcResult = (MvcResult) mockMvc.perform(
                MockMvcRequestBuilders.post("/mall/product-list")
                        .accept(MediaType.ALL)
                        .session(session)
                        .content(reqString)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //得到返回代码
        int status = mvcResult.getResponse().getStatus();
        //得到返回结果
        String content = mvcResult.getResponse().getContentAsString();
        logger.info(content);
    }

    @Test
    void productSummary() throws Exception {
        // geAllBookIDs 获取的列表至少要有一个元素，否则不执行之后的步骤
        Field field = mallController.getClass().getDeclaredField(
                "mallService");
        field.setAccessible(true);
        MallService mallService = (MallService) field.get(mallController);
        if (mallService.getAllBookIDs().size() <= 0) {
            return;
        }
        // 开始测试
        Map map = new HashMap();
        map.put("item_type", "book");
        map.put("id", 1);
        String reqString = MallControllerTest.mapToString(map);
        MvcResult mvcResult = (MvcResult) mockMvc.perform(
                MockMvcRequestBuilders.post("/mall/product-summary")
                        .accept(MediaType.ALL)
                        .session(session)
                        .content(reqString)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //得到返回代码
        int status = mvcResult.getResponse().getStatus();
        //得到返回结果
        String content = mvcResult.getResponse().getContentAsString();
        logger.info(content);
    }

    @Test
    void productDetails() throws Exception {
        // geAllBookIDs 获取的列表至少要有一个元素，否则不执行之后的步骤
        Field field = mallController.getClass().getDeclaredField(
                "mallService");
        field.setAccessible(true);
        MallService mallService = (MallService) field.get(mallController);
        if (mallService.getAllBookIDs().size() <= 0) {
            return;
        }
        // 开始测试
        Map map = new HashMap();
        map.put("item_type", "book");
        map.put("id", 1);
        String reqString = MallControllerTest.mapToString(map);
        MvcResult mvcResult = (MvcResult) mockMvc.perform(
                MockMvcRequestBuilders.post("/mall/product-summary")
                        .accept(MediaType.ALL)
                        .session(session)
                        .content(reqString)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        //得到返回代码
        int status = mvcResult.getResponse().getStatus();
        //得到返回结果
        String content = mvcResult.getResponse().getContentAsString();
        logger.info(content);
    }

    static String mapToString(Map map) {
        ObjectMapper mapper = new ObjectMapper();
        // //设置不写NULLmap值
        // mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

        String outJson = null;
        try {
            outJson = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return outJson;
    }
}