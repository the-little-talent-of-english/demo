package com.englishda.demo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MallServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(MallService.class);

    @Autowired
    private MallService mallService;

    @Test
    void hello() {
        assertAll("hello",
                () -> assertEquals("hello world", mallService.hello())
        );
    }

    @Test
    void getAllBookIDs() {
        List<Integer> res = mallService.getAllBookIDs();
        // System.out.println(res);
        assertAll("getAllBookIDs",
                () -> assertNotEquals(null, res)
        );
    }

    @Test
    void getBookSummaryById() {
        assertAll("getBookSummaryById",
                () -> {
                    if (mallService.getAllBookIDs().size() > 0) {
                        assertNotEquals(null,
                                mallService.getBookSummaryById(1));
                    }
                }
        );
    }

    @Test
    void getBookDetailsById() {
        assertAll("getBookDetailsById",
                () -> {
                    if (mallService.getAllBookIDs().size() > 0) {
                        assertNotEquals(null,
                                mallService.getBookDetailsById(1));
                    }
                }
        );
    }
}