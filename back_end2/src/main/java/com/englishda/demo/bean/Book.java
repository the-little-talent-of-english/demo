package com.englishda.demo.bean;

public class Book {
    private int id;
    private String name;
    private String imgUrl;
    private int price;
    private String info;
    private String bookIntro;
    private String authorIntro;


    @Override
    public String toString() {
        return "Book [ID=" + this.id + ", name=" + this.name +
                ", imgUrl=" + this.imgUrl + ", price=" + this.price + "]";
    }

    public int getID() {
        return this.id;
    }

    public void setID(int value) {
        this.id = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String value) {
        this.imgUrl = value;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int value) {
        this.price = value;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String value) {
        this.info = value;
    }

    public String getBookIntro() {
        return this.bookIntro;
    }

    public void setBookIntro(String bookIntro) {
        this.bookIntro = bookIntro;
    }

    public String getAuthorIntro() {
        return authorIntro;
    }

    public void setAuthorIntro(String authorIntro) {
        this.authorIntro = authorIntro;
    }


}
