package com.englishda.demo.service;


import com.englishda.demo.bean.Book;

import java.util.List;

public interface MallService {
	public String hello();
	List<Integer> getAllBookIDs();
	Book getBookSummaryById(int bookID);
	Book getBookDetailsById(int bookID);
}
