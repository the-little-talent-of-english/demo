package com.englishda.demo.service.impl;

import com.englishda.demo.bean.Book;
import com.englishda.demo.mapper.BookMapper;
import com.englishda.demo.service.MallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MallServiceImpl implements MallService {

    @Autowired
    private BookMapper bookMapper;

    public String hello() {
        return "hello world";
    }

    public List<Integer> getAllBookIDs() {
        return bookMapper.getAllBookIDs();
    }

    public Book getBookSummaryById(int bookID) {
        return bookMapper.getBookSummaryById(bookID);
    }

    public Book getBookDetailsById(int bookID) {
        return bookMapper.getBookDetailsById(bookID);
    }
}
