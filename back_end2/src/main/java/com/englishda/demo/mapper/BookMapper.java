package com.englishda.demo.mapper;

import com.englishda.demo.bean.Book;

import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface BookMapper {
    /**
     * <resultMap id="BookMap" type="com.englishda.demo.bean.Book">
     * <id column="id" property="id"/>
     * <result column="book_name" property="name"/>
     * <result column="info" property="info"/>
     * <result column="book_intro" property="bookIntro"/>
     * <result column="author_intro" property="authorIntro"/>
     * <result column="book_img_url" property="imgUrl"/>
     * <result column="book_price" property="price"/>
     * </resultMap>
     *
     * @return
     */


    @Select("SELECT id FROM book_test")
    List<Integer> getAllBookIDs();

    @Select("SELECT id, book_name, book_img_url, book_price " +
            "FROM book_test WHERE id=#{bookID}")
    @Results(id = "BookMap", value = {
            @Result(column = "id", property = "id", id = true),
            @Result(column = "book_name", property = "name"),
            @Result(column = "info", property = "info"),
            @Result(column = "book_intro", property = "bookIntro"),
            @Result(column = "author_intro", property = "authorIntro"),
            @Result(column = "book_img_url", property = "imgUrl"),
            @Result(column = "book_price", property = "price")
    })
    Book getBookSummaryById(@Param("bookID") int bookID);


    @Select("SELECT " +
            "id, book_name, book_img_url, book_price, " +
            "info, book_intro, author_intro " +
            "FROM book_test WHERE id=#{bookID}")
    @ResultMap("BookMap")
    Book getBookDetailsById(@Param("bookID") int bookID);

}
