import Mock from 'mockjs' // 引入mockjs
import apis from '../apis'

const apiUrl = apis.apiUrl



// mock 使用 example: https://blog.csdn.net/a772304419/article/details/104973585
Mock.mock(
    apiUrl('product-list'),
    'post',
    {
        'ids|10': [ /\d{20}/ ]
    }
) // 根据数据模板生成模拟数据

Mock.mock(
    apiUrl('product-summary'),
    'post',
    {
        'itemType': 'book',
        'itemID': /\d{20}/,
        'name': '@cname', //中文名称
        'points': '@integer(0,10000)',
        'date': '@date(yyyy-mm-dd)',
        'imgUrl': '@image',
    }
)

Mock.mock(
    apiUrl('product-details'),
    'post',
    {
        'itemType': 'book',
        'itemID': /\d{20}/,
        'name': '@cname', //中文名称
        'points': '@integer(0,10000)',
        'date': '@date(yyyy-mm-dd)',
        'imgUrl': '@image',
        'description': '@cparagraph'
    }
)