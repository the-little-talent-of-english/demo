import Vue from 'vue'
import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import App from './App.vue';

import router from './router'
import utils from './utils'
import apis from './apis'

// 语言选项
// import locale from 'element-plus/lib/locale/lang/en'
import locale from 'element-plus/lib/locale/lang/zh-cn'

// import './mock'

const app = createApp(App)
app.use(ElementPlus, { locale })
// -- vue2 --
// app.config.productionTip = false
// app.prototype.$apis = apis 
// app.prototype.$utils = utils
// -- vue2 -> vue3 --
// app.config.globalProperties.$hello = "hello"
// app.config.globalProperties.$apis = apis
// app.config.globalProperties.$utils = utils
// -- vue3 --
// console.log(apis)
app.provide('$hello', "hello") // 成功 inject
app.provide("$apis", apis) // inject 失败
app.provide("$utils", utils)
app.use(router)
app.mount('#app')
