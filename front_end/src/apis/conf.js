const API_MAJOR_VERSION = 0
const API_MINOR_VERSION = 1
const API_PATCH_VERSION = 0


// const API_URL_PREFIX = ``
// const API_URL_PREFIX = `/api/v${API_MAJOR_VERSION}` // for mock
// const API_URL_PREFIX  = `http://localhost:8080/api/v${API_MAJOR_VERSION}/`
// const API_URL_PREFIX = `http://10.63.9.159:8000`
// const API_URL_PREFIX = `http://127.0.0.1:18888`
// const API_URL_PREFIX = `http://127.0.0.1:8080/SSM-0.0.1-SNAPSHOT`
// const API_URL_PREFIX = `http://127.0.0.1:18080`
const API_URL_PREFIX = process.env.VUE_APP_BASE_URL

export default {
    API_MAJOR_VERSION,
    API_MINOR_VERSION,
    API_PATCH_VERSION,

    API_VERSION: `${API_MAJOR_VERSION}.${API_MINOR_VERSION}.${API_PATCH_VERSION}`,
    // const API_URL_PREFIX = `/api/v${API_MAJOR_VERSION}`
    // const API_URL_PREFIX = `http://10.63.9.159:8000`
    API_URL_PREFIX,
}
