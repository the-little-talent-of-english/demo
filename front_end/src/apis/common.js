import axios from 'axios'
import conf from './conf'

// /**
//  * @param {String} path  api path
//  * @returns {String}     url
//  */
// function apiUrl(path) {
//     let p = path
//     if (path.length >= 1) {
//         if (path[0] === '/') p = path.substr(1)
//     }
//     let url = `${conf.API_URL_PREFIX}/${p}`
//     return url
// }

/**
 * 2021-05-22 16:12:25 后缀加了 .do
 * @param {String} path  api path
 * @returns {String}     url
 */
 function apiUrl(path, add_dot_do=false) {
    let p = path
    if (path.length >= 1) {
        if (path[0] === '/') p = path.substr(1)
    }
    let url
    if(add_dot_do){
        url = `${conf.API_URL_PREFIX}/${p}.do`
    }else {
        url = `${conf.API_URL_PREFIX}/${p}`
    }
    return url
}

//创建axios的一个实例
const axiosInstance = axios.create({
    baseURL: '',
    timeout: 6000
})


export default {
    apiUrl,
    axiosInstance,
}