import assert from "assert"
import common from "./common"

const axiosInstance = common.axiosInstance
const apiUrl = common.apiUrl

function fnUrl(path) {
    return apiUrl(`/mall/${path}`, false)
}

/**
 * 获取产品列表
 * 最好只返回 id，等需要显示时才返回更具体的信息
 # @param {String} itemType 对象类型
 * @returns {Promise} 返回一个promise对象
 */
function getProductList({ itemType = null } = {}) {
    assert.ok(itemType != null)
    return axiosInstance({
        method: 'post',
        url: fnUrl('product-list'),
        data: {
            item_type: itemType,
        }
    })
}

/**
 * 获取产品摘要
 */
function getProductSummary({ itemType = null, itemID = null } = {}) {
    assert.ok(itemType != null)
    assert.ok(itemID != null)
    return axiosInstance({
        method: 'post',
        url: fnUrl('product-summary'),
        data: {
            item_type: itemType,
            id: itemID,
        }
    })
}

/**
 * 获取产品详情
 */
function getProductDetails({ itemType = null, itemID = null } = {}) {
    assert.ok(itemType != null)
    assert.ok(itemID != null)
    return axiosInstance({
        method: 'post',
        url: fnUrl('product-details'),
        data: {
            item_type: itemType,
            id: itemID,
        }
    })
}

/**
 * 提交订单
 */
function submitOrderForm(orderForm) {
    assert.ok(orderForm != null)
    return axiosInstance({
        method: 'post',
        url: fnUrl('order-form'),
        data: orderForm
    })
}

export default {
    getProductList,
    getProductSummary,
    getProductDetails,
}