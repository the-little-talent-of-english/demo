import conf from './conf'
import common from './common'
import mall from './mall'


const apiUrl = common.apiUrl
const axiosInstance = common.axiosInstance



// 请求拦截器
axiosInstance.interceptors.request.use(function (request) {
  if (request.method === 'post') {
    // // request.headers['Content-Type'] = 'application/json' // 这会触发 CORS 预检请求
    // request.headers['Content-Type'] = 'text/plain'
    request.headers['Content-Type'] = 'application/json'
    request.data['version'] = conf.API_VERSION
    // 2021-05-22 20:51:11 下面这句似乎还是得加
    // request.headers['Access-Control-Allow-Origin'] = "*"
    request.headers['Access-Control-Allow-Origin'] = "*"
    console.log(request.headers)
  }
  return request;
}, function (error) {
  // 对请求错误做些什么
  console.log('req error');
  return Promise.reject(error);
});

// 响应拦截器
axiosInstance.interceptors.response.use(function (response) {
  return response.data;
}, function (error) {
  // 对响应错误做点什么
  console.log('resp error');
  return Promise.reject(error);
});



export default {
  apiMajorVersion: conf.API_MAJOR_VERSION,
  apiVersion: conf.API_VERSION,
  apiUrlPrefix: conf.API_URL_PREFIX,
  apiUrl,
  mall,

  /**
   * 测试 mock
   */
  helloWorld() {
    // console.log("hello world!")
    // let url = apiUrl('helloWorld')
    let url = apiUrl('hello-world')
    axiosInstance({
      method: 'get',
      url: url,
    }).then((response) => {
      console.log(response)
    }).catch((error) => {
      console.log(error)
    })
  }
}