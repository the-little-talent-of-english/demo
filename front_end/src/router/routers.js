// import Main from '@/components/Main'
// import Mall from '@/components/mall/Mall'
import Mall from '../components/mall/Mall.vue'
import Product from '../components/product/Product.vue'

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
  {
    path: '/',
    redirect: 'mall'
  },
  {
    path: '/mall',
    component: Mall,
  },
  {
    path: '/product',
    component: Product,
  },
]
