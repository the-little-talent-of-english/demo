// import Vue from 'vue'
// import Router from 'vue-router'


// Vue.use(Router)

// let routers = mallRouters

// export default new Router({
//   routes: routers
// })

import { createRouter, createWebHistory } from 'vue-router'
import { routers } from './routers'

const routerHistory = createWebHistory()

const router = createRouter({
  history: routerHistory,
  routes: routers,
})

export default router
