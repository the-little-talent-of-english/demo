module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  transform: {
    '^.+\\.vue$': 'vue-jest'
  },
  reporters: [
    "default",
      [
        "jest-junit",
        {
          // classNameTemplate: (vars) => {
          //   return vars.classname.toUpperCase();
          // }
          suiteName: "jest tests",
          outputDirectory: "./test_reports",
          outputName: "jest-junit.xml",
          uniqueOutputName: "false",
          classNameTemplate: "{classname}-{title}",
          titleTemplate: "{classname}-{title}",
          ancestorSeparator: " › ",
          usePathForSuiteName: true
        }
      ]
  ]
}
