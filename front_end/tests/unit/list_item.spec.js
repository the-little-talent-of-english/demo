import { shallowMount } from '@vue/test-utils'
import ListItem from '@/components/mall/ListItem.vue'
import { wrap } from 'module'

describe('mall/ListItem.vue', () => {
  it('renders props when passed', () => {
    const msg = 'new message'
    const bookName = "bookbookbook"
    const points = 10
    const imgUrl = "https://img2.doubanio.com/view/subject/s/public/s29786993.jpg"
    const wrapper = shallowMount(ListItem, {
      props: {
        name: bookName,
        points: points,
        itemType: "book",
        itemID: 1,
        imgUrl: imgUrl
      }
    })
    const _img = wrapper.find("img")
    expect(_img.attributes("src")).toBe(imgUrl)
    // expect(_img.attributes("src")).toMatch(msg)

    const _itemName = wrapper.find(".item-name")
    expect(_itemName.text()).toMatch(bookName)

    const _itemPoints = wrapper.find(".item-points")
    expect(_itemPoints.text()).toMatch(new RegExp(`${points}\\s*积分`))
  })
})


// props: {
//   name: { type: String, default: "" },
//   points: { type: Number, default: 0 },
//   itemType: { type: [String, null], default: null },
//   itemID: { type: [Number, String, null], default: null },
//   imgUrl: { type: String, default: "" },
// },