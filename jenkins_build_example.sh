# jenkins 提供的 WORKSPACE 与我们实际使用的不同
# Publish JUnit test result report 强制使用这个 WORKSPACE
echo $WORKSPACE # /root/.jenkins/workspace/englishda 
OUR_WORKPLACE=/root/workplace/demo
if [ ! -d "$WORKSPACE/test_reports" ]; then 
    ln -s ${OUR_WORKPLACE}/test_reports $WORKSPACE/test_reports
fi
cd ${OUR_WORKPLACE} && git pull && cd ${OUR_WORKPLACE}/script && sh build.sh && sh test.sh && sh deploy.sh
